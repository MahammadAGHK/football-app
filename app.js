const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const cookieParser = require("cookie-parser");
const dotenv = require("dotenv").config();
const uuid = require("uuid");
const fs = require("fs");
const https = require("https");
app.use(cookieParser())
app.use(bodyParser.urlencoded({extended: true}));

const path = require("path");
const {body,validationResult} = require("express-validator");

var publicdir = path.join(__dirname,"public");
app.use(express.static(publicdir));

app.set('view engine','ejs');

const mysql = require("mysql2");
const { default: axios } = require("axios");

var conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
});

conn.connect((err)=>{
    if (err) throw err;
    console.log("Connected to database successfully");
});


var message ="";
var warning = "";
var valid = "";
app.get("/",(req,res)=>{
    res.render("home");
})

app.get("/register",(req,res)=>{
    res.render("register",{message: "", warning: "", valid: ""});
});

app.post("/register",
    body("username").trim().escape().isLength({min:3,max:15}).withMessage("Username must be minimum 3, maximum 15 letters")
    .matches("^[A-Za-z]*$").withMessage("Username can only contain letters"),
    body("email").isEmail().withMessage("Please enter correct email"),
    body("password").trim().escape().isLength({min: 8}).withMessage("Password must be minimum 8 characters")
    .matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})").withMessage("Enter strong password"),
    (req,res)=>{
        var errors = validationResult(req);
        if (!errors.isEmpty()){
            res.render("register",{message: "",warning: "",valid: errors.array()});
        }
        else{
            if (req.body.password!=req.body.confirmPassword){
                warning = "Passwords don't match";
                res.render("register",{message: "",warning,valid: ""})
            }
            else{
                conn.query("select * from users where username = ?",[req.body.username],(err,result)=>{
                    if (err) throw err;
                    if (result.length){
                        warning = "Username alreday exists";
                        res.render("register",{message: "",warning,valid: ""})
                    }
                    else{
                        bcrypt.hash(req.body.password, 10).then((hashedPassword)=>{
                            conn.query("insert into users(username,email,password) values(?,?,?)",[req.body.username,req.body.email,hashedPassword],(err)=>{
                                if (err) throw err;
                                const sessionToken = uuid.v4();
                                const now = new Date();
                                const expiresat = new Date(+now + 60 * 1000);
                                conn.query("insert into sessions(sessiontoken,username,expiresat) values(?,?,?)",[sessionToken,req.body.username,expiresat],(error)=>{
                                    if (error) throw error;
                                });
                                res.cookie("session_token", sessionToken, { expires: expiresat });
                                res.redirect("/dashboard");
                            });
                        })
                    }
                })
            }
        }
    }
)

app.get("/login",(req,res)=>{
    res.render("login",{message: "",valid: ""});
})

app.post("/login",
    body("username").trim().not().isEmpty().withMessage("Username must be non empty"),
    body("password").trim().not().isEmpty().withMessage("Password must be non empty"),
    (req,res)=>{
        var errors2 = validationResult(req);
        if (!errors2.isEmpty()){
            res.render("login",{message: "",valid: errors2.array()});
        }
        else{
            conn.query("select * from users where username = ?",[req.body.username],(err,result)=>{
                if (err) throw err;
                if (result.length){
                    bcrypt.compare(req.body.password,result[0].password).then((output)=>{
                        if (output){
                            const sessionToken = uuid.v4();
                            const now = new Date();
                            const expiresat = new Date(+now + 5 * 60 * 1000);
                            conn.query("delete from sessions where username = ?",[req.body.username],(error)=>{
                                if (error) throw error;
                            });
                            conn.query("insert into sessions(sessiontoken,username,expiresat) values(?,?,?)",[sessionToken,req.body.username,expiresat],(error)=>{
                                if (error) throw error;
                            });
                            res.cookie("session_token", sessionToken, { expires: expiresat });
                            res.redirect("/dashboard");
                        }
                        else{
                            message = "Username or Password incorrect";
                            res.render("login",{message, valid:""});
                        }
                    })
                }
                else{
                    message = "Username or Password incorrect";
                    res.render("login",{message, valid:""});
                }
            })
        }
    }
)

app.get("/dashboard",(req,res)=>{
    console.log(req.cookies);
    if (!req.cookies){
        res.redirect("/login");
        return;
    }
    const sessionToken = req.cookies['session_token'];
    if (!sessionToken){
        res.redirect("/login");
        return;
    }
    conn.query("select * from sessions where sessiontoken = ?",[sessionToken],(err,result)=>{
        if (err) throw err;
        if (result.length){
            if (result[0].expiresat < (new Date())){
                res.redirect("/login");
            }
            else{
                const newsessionToken = uuid.v4();
                const now = new Date();
                const expiresat = new Date(+now + 5 * 60 * 1000);
                conn.query("insert into sessions(sessiontoken,username,expiresat) values(?,?,?)",[newsessionToken,result[0].username,expiresat],(error)=>{
                    if (error) throw error;
                });
                conn.query("delete from sessions where sessiontoken = ?",[sessionToken],(err2)=>{
                    if (err2) throw err2;
                })
                res.cookie("session_token", newsessionToken, { expires: expiresat });
                conn.query("select * from footballteams where username = ?",[result[0].username],(err1,result1)=>{
                    if (err1) throw err1;
                    conn.query("select * from footballplayers where username = ?",[result[0].username],(err2,result2)=>{
                        if (err2) throw err2;
                        if (result1.length == 0 && result2.length==0){
                            res.render("dashboard",{username: result[0].username,flag:0});
                        }
                        else{
                            res.render("dashboard",{username: result[0].username,flag:1,teams:result1,players:result2});
                        }

                    })
                })
                
            }
        }
        else{
            res.redirect("/login");
            return;
        }
    })

})
app.post("/dashboard",(req,res)=>{
    console.log(req.body);
    if (req.body.delete_team){
        conn.query("delete from footballteams where team_id = ?",[req.body.delete_team],(err)=>{
            if (err) throw err;
            res.redirect("/dashboard");
        })
    }
    if (req.body.delete_player){
        conn.query("delete from footballplayers where id = ?",[req.body.delete_player],(err)=>{
            if (err) throw err;
            res.redirect("/dashboard");
        })
    }
})
app.get("/logout",(req,res)=>{
    if (!req.cookies) {
        res.redirect("/login");
        return;
    }
    const sessionToken = req.cookies['session_token']
    if (!sessionToken) {
        res.redirect("/login");
        return;
    }
    conn.query("delete from sessions where sessiontoken = ?",[sessionToken],(err)=>{
        if (err) throw err;
    })
    res.cookie("session_token", "", { expires: new Date() })
    res.redirect("/login");
});
var apiToken = "rgRV6Nc6QfLby5c8km6rEstnaX9ZlQtytlPxJ2Y235paBEHApW8gbUOhtztO";
var playerdata;
var teamdata;

app.post("/search",(req,res)=>{
    if (req.body.searchChoice){
        if (req.body.searchChoice == "team"){
            https.get(`https://soccer.sportmonks.com/api/v2.0/teams/${Number(req.body.searchTerm)}?api_token=${apiToken}&include=stats&seasons=19799`, (resp) => {
                let data = '';
                resp.on('data', (chunk) => {
                    data+=chunk;
                });
                resp.on('end',()=> {
                    teamdata = JSON.parse(data).data;
                    console.log(teamdata)
                    if (teamdata){
                        
                        var period = teamdata.stats.data[0].scoring_minutes[0].period;
                        var bestminutes = period[0];
                        period.forEach((elem)=>{
                            if (elem.count > bestminutes.count){
                                bestminutes= elem;
                            }
                        })
                        var token = req.cookies['session_token'];
                        conn.query("select * from sessions where sessiontoken = ?",[token],(err,result10)=>{
                            if (err) throw err;
                            conn.query(`insert into footballteams(team_id,founded,bestminutes,logo_path,username,team_name,total_wins,total_losses,total_goals,offsides,corners) values(${teamdata.stats.data[0].team_id},"${teamdata.founded}","${bestminutes.minute}","${teamdata.logo_path}","${result10[0].username}","${teamdata.name}",${teamdata.stats.data[0].win.total},${teamdata.stats.data[0].lost.total},${teamdata.stats.data[0].goals_for.total},${teamdata.stats.data[0].offsides},${teamdata.stats.data[0].total_corners})`,(err)=>{
                                if (err) throw err;
                             })
                        })
                        res.render("team",{message:"",team:teamdata,bestminutes})
                        
                    }
                    else {
                        res.render("team",{message: "No results found"});
                    }
                }).on('error', (err)=>{
                    console.log('Error', err.message);
                });
            })
        }
        if (req.body.searchChoice == "player"){
            https.get(`https://soccer.sportmonks.com/api/v2.0/players/search/${req.body.searchTerm}?api_token=${apiToken}&include=stats,position,team&seasons=19799`, (resp) => {
                let data = '';
                resp.on('data', (chunk) => {
                    data+=chunk;
                });
                resp.on('end',()=> {
                    playerdata = JSON.parse(data).data;
                    if (playerdata.length==1){
                        var token1 = req.cookies['session_token'];
                        conn.query("select * from sessions where sessiontoken = ?",[token1],(err,result10)=>{
                            if (err) throw err;
                            conn.query(`insert into footballplayers(player_name,country,birthdate,position,image_path,username,height) values("${playerdata[0].fullname}","${playerdata[0].birthcountry}","${playerdata[0].birthdate}","${playerdata[0].position.data.name}","${playerdata[0].image_path}","${result10[0].username}","${playerdata[0].height}")`,(err)=>{
                                if (err) throw err;
                            })
                        })
                        res.render("player",{message:"",flag:0,player:playerdata[0]})
                    }
                    else if (playerdata.length > 1){
                        res.render("player",{message:"",flag:1,players: playerdata })
                    }
                    else {
                        res.render("player",{message: "No results found"});
                    }
                }).on('error', (err)=>{
                    console.log('Error', err.message);
                });
            })
        }
    }
    if (req.body.playerid){
        
        var player = playerdata.filter((elem)=>{
            return elem.player_id==req.body.playerid;
        })
        var token = req.cookies['session_token'];
        conn.query("select * from sessions where sessiontoken = ?",[token],(err,result10)=>{
            if (err) throw err;
            conn.query(`insert into footballplayers(player_name,country,birthdate,position,image_path,username,height) values("${player[0].fullname}","${player[0].birthcountry}","${player[0].birthdate}","${player[0].position.data.name}","${player[0].image_path}","${result10[0].username}","${player[0].height}")`,(err)=>{
                if (err) throw err;
            })
        })
        res.render("player",{message:"",flag:0,player:player[0]})
    }
})

app.listen("9000",(err)=>{
    if (err) throw err;
    console.log("server started successfully");
})