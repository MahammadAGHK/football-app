create database webproject;

use webproject;

create table users(
    id int primary key auto_increment,
    username varchar(15) not null,
    email varchar(40) not null,
    password varchar(80) not null
);

create table sessions(
    sessiontoken varchar(50) primary key,
    username varchar(15) not null,
    expiresat datetime not null
);

create table footballplayers(
    id int auto_increment primary key,
    player_name varchar(70),
    country varchar(30),
    birthdate varchar(20),
    height varchar(10),
    position varchar(15),
    image_path varchar(100),
    username varchar(15)
);

create table footballteams(
    id int auto_increment primary key,
    team_id int,
    team_name varchar(30),
    founded varchar(5),
    total_wins int,
    total_losses int,
    total_draws int,
    total_goals int,
    bestminutes varchar(10),
    offsides int,
    corners int,
    logo_path varchar(100),
    username varchar(15)
);